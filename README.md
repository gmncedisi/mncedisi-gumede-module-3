# mncedisi_gumede_module3

Mncedisi Gumede

MTN APP Academy Screen
Create a Flutter App that has the following screens navigating to each other. (6 Screens)

   1. Login and registration screens (not linked to a database) with relevant input fields.
   2. Dashboard (the screen after login) with buttons to feature screens of your app.
   3. The last screen must be of a user profile edit.
   4. Dashboard Floating button on the home screen, linking to another screen.
   5. Each screen must be labeled appropriately on the app bar.

![Sematic description of image](/welcome_screen.png)
![Sematic description of image](/sign_in_screen.png)
![Sematic description of image](/sign_up_screen.png)
![Sematic description of image](/dashboard.png)
![Sematic description of image](/about_screen.png)
![Sematic description of image](/profile_screen.png)

